<?php

namespace Drupal\sitetour\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element to display a site tour js.
 *
 * @RenderElement("sitetour")
 */
class SiteTourTheming extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {    
    return [
      '#attached' => [
        'library' => [
          'sitetour/sitetour_js',
        ],
      ],
      '#data' => [],
      '#type' => [],     
      '#pre_render' => [
        [self::class, 'preRenderSiteTourTheming'],
      ],
      '#theme' => 'sitetour',
    ];
  }

  /**
   * Element pre render callback.
   */
  public static function preRenderSiteTourTheming($element) {
    $element['#attached']['drupalSettings']['sitetour'] = [      
      'data' => $element['#data'],       
    ];
    return $element;
  }

}
