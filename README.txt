CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

  Site tour is a javascript API to integrate with Hopscotch library.  
  It provides a render element to generate a tour pop up from custom block.
  
  You can find more information about this plugin in the below link,
   - https://github.com/LinkedInAttic/hopscotch

  Usage : 

    This module require parameters to pass to the render element. 
    Create a custom block plugin and pass the value in the below
    format as return value.

    $tour = [];
    $build['sitetour']['#data']['tour'] = [];
    $build['sitetour']['#type'] = 'sitetour';
    $tour['id'] = 'sitetour';
    $tour['steps'][0]['target'] = 'slide-show'; // Dom element id
    $tour['steps'][0]['title'] = 'Slide show';
    $tour['steps'][0]['content'] = 'It includes five images. Please click the next button to view the next slide image.';
    $tour['steps'][0]['placement'] = 'bottom';
    $tour['steps'][0]['arrowOffset'] = 60;

    $tour['steps'][1]['target'] = 'search';
    $tour['steps'][1]['title'] = 'Search Box'; // Dom element id
    $tour['steps'][1]['content'] = 'Please enter the text in the search box and click enter button to search.';
    $tour['steps'][1]['placement'] = 'bottom';
    $tour['steps'][1]['arrowOffset'] = 60;
    $build['sitetour']['#data']['tour'] = $tour;

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the Site tour module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

 This module does not require any configuration.


MAINTAINERS
-----------

Current maintainers:
  Elavarasan R - https://www.drupal.org/user/1902634
