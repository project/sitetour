/**
 * @file
 * Site Tour custom js file.
 */
(function ($, Drupal, drupalSettings) {
  var flag = false; 
  Drupal.behaviors.sitetour = {
    attach: function (context, settings) {
      if(!flag) {
        var tour = drupalSettings.sitetour.data.tour;
        init = function() {
          hopscotch.startTour(tour);          
        };
        flag = true;
        init();
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
